# README #

Magshichess - the crapiest chess engine ever made.

### What is does this repo contains? ###

* An backend engine for managing a chess game (which his frontend built for windows)
* Version 1.0.0

### How do I get set up? ###

* Download chessGraphics.exe from the download section (https://bitbucket.org/bayadsters/maghichess/downloads/chessGraphics.exe)
* Compile on a windows machine

### Contribution guidelines ###

* You can help us distribute our project to other platforms such as Linux or Mac OS.

### Whats Next? ###

* A CLI version of the game

#################################
Icons made by Freepik from www.flaticon.com