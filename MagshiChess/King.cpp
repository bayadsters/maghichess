#include "King.h"
#include "Board.h"

/*
King class default c'tor
*/
King::King(Position& pos, char type, bool color, Board* board)
	: Piece(pos, type , color ,board)
{
	this->_movments = 
	{
		Position(-1 , 0),
		Position(0 , 1),
		Position(1 , 0),
		Position(0 , -1),
		Position(1 , 1), 
		Position(-1 , -1), 
		Position(1 , -1),
		Position(-1 , 1)
	};
}


King::~King()
{
}



/*
This method checks of the king can move accoeding to his special route
Input: the new position
Output: if he can move
*/
int King::checkMove(Position& pos)
{
	int response = response::error::ILLEGAL_MOVEMENT;
	Position tmp(this->_pos);
	int i = 0;


	for (Position& i : this->_movments)
	{
		tmp += i;

		if (tmp == pos)
		{
			response = response::succes::MOVEMENT;
			break;
		}
		else
		{
			tmp = this->_pos;
		}
	}


	return response;
}

