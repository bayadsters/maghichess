#ifndef KING_H
#define KING_H

#include "Piece.h"
#include "Position.h"

class King : public Piece
{
	public:
		King(Position& pos, char type, bool color, Board* board);
		~King();

		int checkMove(Position&) override;
};



#endif //!KING_H
