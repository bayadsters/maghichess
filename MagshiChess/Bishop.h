#ifndef BISHOP_H
#define BISHOP_H

#include "Piece.h"
#include "Position.h"

class Bishop : public Piece
{
	public:
		Bishop(Position& pos, char type, bool color, Board* board);
		~Bishop();

		int checkMove(Position&) override;
};

#endif // !BISHOP_H
