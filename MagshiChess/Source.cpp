#include "Pipe.h"
#include "Board.h"
#include <iostream>
#include <thread>

using namespace std;

void main()
{
	srand(time_t(NULL));

	try
	{
		Board b("rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0");
		Pipe p;
		bool isConnect = p.connect();
		
		
		string ans;
		while (!isConnect)
		{
			cout << "cant connect to graphics" << endl;
			cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
			cin >> ans;

			if (ans == "0")
			{
				cout << "trying connect again.." << endl;
				Sleep(5000);
				isConnect = p.connect();
			}
			else 
			{
				p.close();
				return;
			}
		}
	

		char msgToGraphics[1024];
		strcpy_s(msgToGraphics, b.toString().c_str());
		
		p.sendMessageToGraphics(msgToGraphics);   // send the board string

		// get message from graphics
		string msgFromGraphics = p.getMessageFromGraphics();

		while (msgFromGraphics != "quit")
		{
			// should handle the string the sent from graphics
			// according the protocol. Ex: e2e4           (move e2 to e4)
			
			// YOUR CODE
			
			strcpy_s(msgToGraphics,  std::to_string(b.move(msgFromGraphics)).c_str()); // msgToGraphics should contain the result of the operation
			cout << endl;
			b.print();


			// return result to graphics		
			p.sendMessageToGraphics(msgToGraphics);   

			// get message from graphics
			msgFromGraphics = p.getMessageFromGraphics();
		}

		p.close();
	}
	catch (invalid_argument& e)
	{
		cout << "ERROR: " << e.what() << endl;
		return;
	}
}