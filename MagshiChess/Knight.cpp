#include "Knight.h"
#include "Board.h"


/*
Knight default class c'tor
Input: Piece c'tor parameters
*/
Knight::Knight(Position& pos, char type, bool color, Board* board)
	: Piece(pos, type, color, board)
{
	this->_movments = 
	{
		Position(-2 , 1),
		Position(-1 , 2),

		Position(2 , 1),
		Position(1 , 2),

		Position(-1 , -2),
		Position(-2 , -1),

		Position(1 , -2),
		Position(2 , -1)
	};

}


Knight::~Knight()
{
}


/*
This method checks if the knigt can move correctly
Input: the new position
Output: response code
*/
int Knight::checkMove(Position& dstPos)
{
	int response = response::error::ILLEGAL_MOVEMENT;
	int i = 0;
	Position tmp(this->_pos);

	for (Position& i : this->_movments)
	{
		tmp += i;

		if (tmp == dstPos)
		{
			response = response::succes::MOVEMENT;
			break;
		}
		else tmp = this->_pos;
	}

	return response;
}
