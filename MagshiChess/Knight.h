#ifndef KNIGHT_H
#define KNIGHT_H

#include "Piece.h"
#include "Position.h"

class Knight : public Piece
{
	public:
		Knight(Position& pos, char type, bool color, Board* board);
		~Knight();

		int checkMove(Position&) override;
};



#endif // !KNIGHT_H
