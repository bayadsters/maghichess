#ifndef QUEEN_H
#define QUEEN_H

#include "Rook.h"
#include "Bishop.h"
#include "Piece.h"

//class Queen : public Rook, public Bishop
class Queen : public Piece //just so it will compile
{
	public:
		Queen(Position& pos, char type, bool color, Board* board);
		~Queen();
	
		int checkMove(Position&) override;
};


#endif // !QUEEN_H
