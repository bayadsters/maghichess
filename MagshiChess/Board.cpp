#include "Board.h"
#include <iostream>

using std::cout;
using std::endl;


/*
Board class default constructor
Input: the board representation as string
*/
Board::Board(string board)
{
	int i = 0 , j = 0;
	Piece* tmp = nullptr;
	Position pos(-1,7);
	bool advanceRes;

	while (board[i] != NULL)
	{
		advanceRes = pos.advance();

		switch (tolower(board[i]))
		{
			case KING_NOTATION:
				tmp = new King(pos, KING_NOTATION, islower(board[i]), this);
				break;

			case QUEEN_NOTATION:
				tmp = new Queen(pos, QUEEN_NOTATION, islower(board[i]), this);
				break;

			case ROOK_NOTATION:
				tmp = new Rook(pos, ROOK_NOTATION, islower(board[i]), this);
				break;

			case KNGIHT_NOTATION:
				tmp = new Knight(pos, KNGIHT_NOTATION, islower(board[i]), this);
				break;

			case BISHOP_NOTATION:
				tmp = new Bishop(pos, BISHOP_NOTATION, islower(board[i]), this);
				break;

			case PAWN_NOTATION:
				tmp = new Pawn(pos, PAWN_NOTATION, islower(board[i]), this);
				break;
			case EMPTY_NOTATION:
				break;

			case WHITE_TURN:
				this->_turn = WHITE;
				break;

			case BLACK_TURN:
				this->_turn = BLACK;
				break;

			default:
				throw std::invalid_argument("Not supported type");
				break;
		}

		if (advanceRes)
		{
			this->_pieces[pos.getNum()][pos.getLetter()] = tmp;
		}

		i++;
		tmp = nullptr;
	}
}


/*
Board class d'tor
*/
Board::~Board()		
{
	for (int i = 0; i < CHESS_ROW_SIZE; i++)
	{
		for (int k = 0; k < CHESS_ROW_SIZE; k++)
		{
			if(this->_pieces[i][k]) delete this->_pieces[i][k];
		}
	}
}


/*
This method returns the current turn
Input: none
Output: the current turn
*/
bool Board::getTurn()
{
	return this->_turn;
}


/*
This method returns the board represented as a string
Input: none
Output: the board as string
*/
string Board::toString()
{
	string boardStr = "";

	for (int i = CHESS_ROW_SIZE - 1; i >= 0; i--)
	{
		for (int k = 0; k < CHESS_ROW_SIZE; k++)
		{
			if (this->_pieces[i][k])
			{
				boardStr += (this->_pieces[i][k])->getType();
			}
			else
			{
				boardStr += "#";
			}
		}
	}

	boardStr += this->_turn ? "1" : "0";
	return boardStr;
}


/*
This method checks if a chess move is avaible to be done for a certain color
Input: the color
Output: if chess move can be done
*/
bool Board::checkChess(bool color)
{
	int response = -1;
	vector<Position> v = this->getTypePos(KING_NOTATION , color);

	for (int i = 0; i < CHESS_ROW_SIZE; i++)
	{
		for (int j = 0; j < CHESS_ROW_SIZE; j++)
		{
			if (this->_pieces[i][j])
			{
				if (this->_pieces[i][j]->getColor() != color) //Players from other color
				{
					for (Position& p : v)
					{
						response = this->_pieces[i][j]->checkMove(p);
						if (response == response::succes::MOVEMENT) return true;
					}
				}
			}
		}
	}

	return false;
}


/*
This method moves the Piece at a certain position according to request string
(this is the interface of moving pieces, it checks for errors and then calls for the 
inner private move method)
Input: request
Output: response
*/
int Board::move(string request)
{
	try
	{
		Piece* src = this->at(request.substr(0, 2));
		Position dstPos(request.substr(2, 2));

		if (!src) return response::error::NOT_PLAYER_PIECE;

		return move(src, dstPos);
	}
	catch (const invalid_argument) 
	{
		return response::error::ILLEGAL_SQUERES;
	}
}


/*
This method moves the piece to a new position on the board (if it can)
(private method)
Input: the piece and the wanted position
Output: respone code
*/
int Board::move(Piece* src, Position& dstPos)
{
	int response = -1;
	Piece* tmp = nullptr;
	Position srcPos = src->getPos();
	bool eat = false;


	if (src->getColor() != this->_turn) //if it is not the piece turn
	{
		response = response::error::NOT_PLAYER_PIECE;
	}
	else if (this->at(dstPos)) //if there is a player in the destenation position
	{
		if (dstPos == src->getPos())
		{
			response = response::error::SRC_DST_SAME;
		}
		else if (this->at(dstPos)->getColor() == src->getColor())
		{
			response = response::error::PLAYER_PIECE_DST;
		}
		else
		{
			eat = true;
		}
	}
	if (response == -1) //if the response have not changed then
	{
		response = src->checkMove(dstPos);

		if (response == response::succes::MOVEMENT)
		{
			tmp = this->at(dstPos);
			src->setPos(dstPos);
			
			this->_pieces[srcPos.getNum()][srcPos.getLetter()] = nullptr;
			this->_pieces[dstPos.getNum()][dstPos.getLetter()] = src;


			if(checkChess(this->_turn))
			{
				src->setPos(srcPos);
				this->_pieces[dstPos.getNum()][dstPos.getLetter()] = tmp;
				this->_pieces[srcPos.getNum()][srcPos.getLetter()] = src;


				response = response::error::CAUSE_CURR_PLAYER_CHESS;
			}
			else if (checkChess(!this->_turn))
			{
				response = response::succes::CHESS;
				this->_turn = !this->_turn;
			}
			else
			{
				this->_turn = !this->_turn;
			}


			if (eat)
			{
				delete tmp; //if the piece eats other piece
			}

		}
	}
	
	
	return response;
}


/*
This method returns the piece at a certain position
Input: the string which is the position request 
Outout: pointer to the piece
*/
Piece* Board::at(string str)
{
	return Board::at(Position(str));
}


/*
This method returns the piece at a certain position
Input: the position request
Outout: pointer to the piece
*/
Piece* Board::at(Position& pos)
{
	return this->_pieces[pos.getNum()][pos.getLetter()];
}


/*
This method returns a vector of the position of all of the pieces from one type and color
Input: the type
Output: the vector
*/
vector<Position> Board::getTypePos(char type , bool color)
{
	vector<Position> v;
	int i = 0, j = 0;
	
	for (i = 0; i < CHESS_ROW_SIZE; i++)
	{
		for (j = 0; j < CHESS_ROW_SIZE; j++)
		{
			if (this->_pieces[i][j])
			{
				if (this->_pieces[i][j]->getType() == type && this->_pieces[i][j]->getColor() == color)
				{
					v.push_back(this->_pieces[i][j]->getPos());
				}
			}
		}
	}
	
	return v;
}


/*
This method finds out if point is taken(regardless of color)
Input: position
Output: if the point is taken
*/
bool Board::isTaken(Position& point)
{
	return _pieces[point.getNum()][point.getLetter()];
}


/*
This method finds out if point is taken by a piece in the specified color
Input: position and color
Output: if the point is taken by the specific color piece
*/
bool Board::isTaken(Position &point , bool color)
{
	if (_pieces[point.getNum()][point.getLetter()])
	{
		return  _pieces[point.getNum()][point.getLetter()]->getColor() == color;
	}
	else return false;
}


/*
This method prints the board
Input: none
Output: none
*/
void Board::print()
{
	for (int i = CHESS_ROW_SIZE - 1; i >= 0; i--)
	{
		for (int j = 0; j < CHESS_ROW_SIZE; j++)
		{	
			if (this->_pieces[i][j])
			{
				cout << this->_pieces[i][j]->getType() << " ";
			}
			else
			{
				cout << "# ";
			}
		}

		cout << endl;
	}
}
