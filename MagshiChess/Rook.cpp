#include "Rook.h"
#include "Board.h"

enum directions 
{
	RIGHT = 0,
	UP,
	LEFT,
	DOWN
};


Rook::Rook(Position& pos, char type, bool color, Board* board)
	: Piece(pos , type , color ,board)
{
	this->_movments = {
		Position(1,0),
		Position(0,1),
		Position(-1,0),
		Position(0,-1)
	};
}


Rook::~Rook()
{
}


/*
This method checks if the rook can move correctly 
Input: the new position
Output: response code
*/
int Rook::checkMove(Position& pos) //Huge bug with the first row!
{
	int response = -1; 

	bool letterEq = (pos.getLetter() == this->_pos.getLetter()); 
	bool numEq = (pos.getNum() == this->_pos.getNum());
	bool isPosEq;
	int dirChoise = -1;
	Position tmp(this->_pos); //temp position so that the rook position wont change



	if (letterEq)
	{
		this->_pos.getNum() > pos.getNum() ? dirChoise = directions::DOWN : dirChoise = directions::UP;
	}
	else if (numEq)
	{
		this->_pos.getLetter() > pos.getLetter() ? dirChoise = directions::LEFT : dirChoise = directions::RIGHT;
	}
	else
	{
		return response::error::ILLEGAL_MOVEMENT;
	}

	isPosEq = (tmp == pos);

	while (!isPosEq)
	{
		tmp += this->_movments[dirChoise];

		if (tmp != pos && this->_board->at(tmp))
		{
			return response::error::ILLEGAL_MOVEMENT; //if there is a piece in the way
		}

		isPosEq = (tmp == pos);
	}


	return response::succes::MOVEMENT;
}
