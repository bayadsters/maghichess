#ifndef POSITION_H
#define POSITION_H

#include <string>
#include <exception>

using std::string;
using std::invalid_argument;
using std::exception;

class Position
{
	private:
		int _letter;
		int _num;

	public:
		Position(char*);
		Position(string&);
		Position(int letter, int num);
		Position(const Position&);
		~Position();

		int getNum();
		int getLetter();

		string toString() const;
		void move(int, int);
		bool advance();
		double getSlope(const Position&) const;


		Position& operator=(const Position&);
		bool operator==(string&) const;
		bool operator==(const Position&) const;
		bool operator!=(const Position&) const;
		bool operator+=(const Position&);
		bool operator++();
		bool operator--();
};



#endif // !POSITION_H