#include "Queen.h"
#include "Board.h"

enum directions
{
	RIGHT = 0,
	UP,
	LEFT,
	DOWN,
	UP_LEFT,
	UP_RIGHT,
	DOWN_LEFT,
	DOWN_RIGHT
};

Queen::Queen(Position& pos, char type, bool color, Board* board)
	: Piece::Piece(pos, type, color, board)	
{
	this->_movments =
	{
		Position(1,0),
		Position(0,1),
		Position(-1,0),
		Position(0,-1),
		Position(-1 , 1),
		Position(1 , 1),
		Position(-1 , -1),
		Position(1 , -1)
	};
}

Queen::~Queen()
{
}

int Queen::checkMove(Position& pos)
{
	try
	{
		bool letterEq = (pos.getLetter() == this->_pos.getLetter());
		bool numEq = (pos.getNum() == this->_pos.getNum());
		bool isPosEq;
		
		int dirChoise = -1;
		Position tmp(this->_pos); //temp position so that the rook position wont change

		if (letterEq)
		{
			this->_pos.getNum() > pos.getNum() ? dirChoise = directions::DOWN : dirChoise = directions::UP;
		}
		else if (numEq)
		{
			this->_pos.getLetter() > pos.getLetter() ? dirChoise = directions::LEFT : dirChoise = directions::RIGHT;
		}
		else
		{
			int slope = this->getPos().getSlope(pos);
			if (slope == 1)
			{
				this->_pos.getLetter() < pos.getLetter() ? dirChoise = directions::UP_RIGHT : dirChoise = directions::DOWN_LEFT;
			}
			else if (slope == -1)
			{
				this->_pos.getLetter() < pos.getLetter() ? dirChoise = directions::DOWN_RIGHT : dirChoise = directions::UP_LEFT;
			}
			else
			{
				return response::error::ILLEGAL_MOVEMENT;
			}
		}

		isPosEq = (tmp == pos);

		while (!isPosEq)
		{
			tmp += this->_movments[dirChoise];

			if (tmp != pos && this->_board->at(tmp))
			{
				return response::error::ILLEGAL_MOVEMENT; //if there is a piece in the way
			}

			isPosEq = (tmp == pos);
		}

		return response::succes::MOVEMENT;
	}
	catch (exception)
	{
		return response::error::ILLEGAL_MOVEMENT;
	}
}
