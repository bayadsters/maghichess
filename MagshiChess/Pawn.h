#ifndef PAWN_H
#define PAWN_H

#include "Piece.h"

class Pawn : public Piece
{
	private:
		bool first_move;

	public:
		Pawn(Position& pos, char type, bool color, Board* board);
		~Pawn();

		int checkMove(Position&) override;
};

#endif // !PAWN_H