#include "Position.h"

#define NUM_POS 1
#define CHR_POS 0

//Copy-paste Position::Position(string)
Position::Position(char* str)
	: Position(std::string(str))
{
}


//Position default class c'tor
//Input: string
Position::Position(string& input)
{
	if (input.length() == 2)
	{
		this->_letter = input[CHR_POS] - 'a';
		this->_num = input[NUM_POS] - '0' - 1;
		
		if (_letter > 7 || _num > 7 || _letter < 0 || _num < 0)
		{
			throw invalid_argument("out of board");
		}
	}
	else
	{
		throw invalid_argument("Illegal length");
	}

}


/*
for movement vector initialization only!
use with caution because there is no checking
*/
Position::Position(int letter, int num) : _letter(letter) , _num(num)
{
	if (letter > 7 || num > 7)
	{
		throw invalid_argument("out of board");
	}
}


/*
Position copy class c'tor
Input: other position
*/
Position::Position(const Position& other)
{
	this->_letter = other._letter;
	this->_num = other._num;
}


//Position d'tor
Position::~Position()
{
}


int Position::getNum()
{
	return _num;
}


int Position::getLetter()
{
	return _letter;
}


/*
This method converts the position object to a string
Input: none
Output: the string
*/
string Position::toString() const
{
	string pos = std::to_string(this->_num);
	pos += std::to_string(this->_letter);
	return pos;
}


/*
This method moves the position by the parameters
Input: letter increment and number increment
Output: none
*/
void Position::move(int letter, int num)
{
	if (!(letter + this->_letter > 7 || num + this->_num > 7 || letter + this->_letter < 0 || num + this->_num < 0))
	{
		this->_letter += letter;
		this->_num += num;
	}
	else
	{
		throw exception("out of board");
	}
}


/*
advances the position goind down in rows but up in collums (for array uses)
Input: none
Output: to stop or not
*/
bool Position::advance()
{
	if (!(this->_num == 0 && this->_letter == 7))
	{
		if (this->_letter == 7)
		{
			this->_letter = 0;
			this->_num--;
		}
		else
		{
			this->_letter++;
		}

		return true;
	}
	else
	{
		return false;
	}
}


/*
This methoed returns the slope between two this and other position 
Input: other position
Output: the slope between this and other

According to the formula: m = y2 - y1 / x2 - x1

*/
double Position::getSlope(const Position& other) const
{
	double ptOne = other._num - this->_num;
	double ptTwo = other._letter - this->_letter;
	

	if (ptTwo == 0)
	{
		throw exception("Division by zero");
	}
	else return ptOne / ptTwo;
}


/*
Position '=' operator overloading
Input: a position reference
Output: position which is the sum of this and the other
*/
Position& Position::operator=(const Position& other)
{
	if (this)
	{
		this->_letter = other._letter;
		this->_num = other._num;
	}

	return *this;
}


/*
Position '==' operator overloading
Input: a string
Output: if the object is equal to the string
*/
bool Position::operator==(string& other) const
{
	Position tmp(other);
	return tmp == *this;
}


/*
Position '==' operator overloading
Input: a position
Output: if the object is equal to the other position
*/
bool Position::operator==(const Position& other) const
{
	return this->_letter == other._letter && this->_num == other._num;
}


/*
Position '!=' operator overloading
Input: a position
Output: if the object is not equal to the other position
*/
bool Position::operator!=(const Position& other) const
{
	return this->_letter != other._letter || this->_num != other._num;
}


/*
Position '+=' operator overloading
Input: a position
Output: a new position made of adding both positions
*/
bool Position::operator+=(const Position& other)
{
	if (!(other._letter + this->_letter > 7 || other._num + this->_num > 7 || other._letter + this->_letter < 0 || other._num + this->_num < 0))
	{
		_num += other._num;
		_letter += other._letter;
		return true;
	}
	else
	{
		return false;
	}
}


/*
Position '++' operator overloading (increments by 1)
Input: none
Output: none
*/
bool Position::operator++()
{
	if (!(this->_num == 7 && this->_letter == 7))
	{
		if (this->_letter == 7)
		{
			this->_letter = 0;
			this->_num++;
		}
		else
		{
			this->_letter++;
		}		
		return true;
	}
	else
	{
		return false;
	}
}


/*
Position '--' operator overloading (decrements by 1)
Input: none
Output: none
*/
bool Position::operator--()
{
	if (!(this->_num == 0 && this->_letter == 0))
	{
		if (this->_letter == 0)
		{
			this->_letter = 7;
			this->_num--;
		}
		else
		{
			this->_letter--;
		}
		return true;
	}
	else
	{
		return false;
	}
}