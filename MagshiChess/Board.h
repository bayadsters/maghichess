#ifndef BOARD_H
#define BOARD_H

#include "Piece.h"
#include "Position.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Queen.h"
#include "Rook.h"
#include "Pawn.h"
#include <string>
#include <exception>
#include <vector>
#include <ctype.h>

#define CHESS_ROW_SIZE 8

/////////////////Notations///////////
#define KING_NOTATION 'k'
#define QUEEN_NOTATION 'q'
#define ROOK_NOTATION 'r'
#define KNGIHT_NOTATION 'n'
#define BISHOP_NOTATION 'b'
#define PAWN_NOTATION 'p'
#define EMPTY_NOTATION '#'
#define WHITE_TURN '0'
#define BLACK_TURN '1'
////////////////////////////////////

////////////Colors//////////////////
#define WHITE false
#define BLACK true


using std::string;
using std::vector;
class Piece;

class Board
{
	private:
		Piece* _pieces[CHESS_ROW_SIZE][CHESS_ROW_SIZE];
		bool _turn;

		int move(Piece* piece, Position& pos);

	public:
		Board(string);
		~Board();

		bool getTurn();
		string toString();
		bool checkChess(bool color);
		int move(string request);
		Piece* at(string); //Returns the piece at the place
		Piece* at(Position&);
		vector<Position> getTypePos(char type , bool color);
		bool isTaken(Position&);
		bool isTaken(Position&, bool);
		
		void print();
};


#endif // !BOARD_H