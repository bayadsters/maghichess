#include "Piece.h"


/*
Piece c'tor
Input: position , type and the board
*/
Piece::Piece(Position& pos, char type , bool color, Board* board)
	: _pos(pos)
{
	this->_type = color ? type : toupper(type);
	this->_board = board;
	this->_color = color;
}


//Piece d'tor
Piece::~Piece()
{
	this->_movments.clear();
}


/*
This method sets a new position to the piece
Input: new position
Output: none
*/
void Piece::setPos(Position& newPos)
{
	this->_pos = newPos;
}


/*
This method returns the position of the piece
Input: none
Output: the position
*/
const Position& Piece::getPos() const
{
	return this->_pos;
}


/*
This method returns the color of the piece
Input: none
Output: true for black flase for white
*/
bool Piece::getColor() const
{
	return this->_color;
}


/*
This method returns the type of the piece
Input: none
Output: the type
*/
char Piece::getType() const
{
	return this->_type;
}
