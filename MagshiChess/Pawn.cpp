#include "Pawn.h"
#include "Board.h"


Pawn::Pawn(Position& pos, char type, bool color, Board* board)
	: Piece(pos , type , color , board)
{
	this->first_move = true;

	if (color)
	{
		this->_movments =
		{
			Position(0 , -1),
			Position(0 , -2),
			Position(1, -1),
			Position(-1 , -1)
		};
	}
	else
	{

		this->_movments =
		{
			Position(0 , 1),
			Position(0 , 2),
			Position(-1, 1),
			Position(1, 1)
		};
	}

}



Pawn::~Pawn()
{
}



/*
This method cheks if the pawn can move correctly
Input: the new position
Output: response code
*/
int Pawn::checkMove(Position& pos)
{
	int response = -1 , i = 0;
	Position tmp(this->_pos);


	if (this->_board->isTaken(pos , !this->getColor()))
	{
		i = 2; //start index of eating positions
		while (tmp != pos && i < 4)
		{
			tmp = this->_pos;
			tmp += this->_movments[i];
			i++;
		}
	}
	else if (this->first_move)
	{
		while (tmp != pos && i < 2)
		{
			tmp = this->_pos;
			tmp += this->_movments[i];
			i++;
		}
	}

	else
	{
		tmp += this->_movments[0];
	}


	if (tmp != pos)
	{
		response = response::error::ILLEGAL_MOVEMENT; //cant find the squere requested
	}
	else
	{
		response = response::succes::MOVEMENT;
		if (this->first_move) this->first_move = false;
	}

	return response;
}

