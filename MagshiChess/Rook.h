#ifndef ROOK_H
#define ROOK_H

#include "Piece.h"
#include "Position.h"

class Rook : public Piece
{
	public:
		Rook(Position& pos, char type, bool color, Board* board);
		~Rook();

		int checkMove(Position&) override;
};

#endif // !ROOK_H