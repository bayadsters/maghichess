#ifndef PIECE_H
#define PIECE_H

#include "Position.h"
#include <vector>

namespace response 
{
	enum succes 
	{
		MOVEMENT = 0,
		CHESS
	};

	enum error 
	{
		NOT_PLAYER_PIECE = 2,
		PLAYER_PIECE_DST,
		CAUSE_CURR_PLAYER_CHESS,
		ILLEGAL_SQUERES,
		ILLEGAL_MOVEMENT,
		SRC_DST_SAME
	};
}

class Board;
using std::vector;

class Piece
{
	private:
		bool _color;

	protected:
		char _type;
		Position _pos;
		Board* _board;
		vector<Position> _movments;

	public:
		Piece(Position& pos, char type, bool color ,Board* board);
		virtual ~Piece();

		void setPos(Position& newPos);
		virtual int checkMove(Position&) = 0;
		const Position& getPos() const;
		bool getColor() const;
		char getType() const;
};

#endif // !PIECE_H
