#include "Bishop.h"
#include "Board.h"

enum directions 
{
	UP_LEFT = 0,
	UP_RIGHT,
	DOWN_LEFT,
	DOWN_RIGHT
};


/*
Bishop default c'tor
Input: Position default params
*/
Bishop::Bishop(Position& pos, char type, bool color, Board* board)
	: Piece(pos , type , color , board)
{
	this->_movments =
	{
		Position(-1 , 1),
		Position(1 , 1),
		Position(-1 , -1),
		Position(1 , -1)
	};

}


Bishop::~Bishop()
{
}


/*
This method checks of the bishop can move accoeding to his special route
Input: the new position
Output: if he can move
*/
int Bishop::checkMove(Position& dstPos)
{
	try
	{
		int response = response::error::ILLEGAL_MOVEMENT;
		int i = 0;
		Position tmp(this->_pos);
		int slope = this->getPos().getSlope(dstPos);
		int dirChoise = -1;

		if (slope == 1)
		{
			this->_pos.getLetter() < dstPos.getLetter() ? dirChoise = directions::UP_RIGHT : dirChoise = directions::DOWN_LEFT;
		}
		else if (slope == -1)
		{
			this->_pos.getLetter() < dstPos.getLetter() ? dirChoise = directions::DOWN_RIGHT : dirChoise = directions::UP_LEFT;
		}
		else
		{
			return response::error::ILLEGAL_MOVEMENT;
		}

		bool isPosEq = (tmp == dstPos);

		while (!isPosEq)
		{
			tmp += this->_movments[dirChoise];

			if (tmp != dstPos && this->_board->at(tmp))
			{
				return response::error::ILLEGAL_MOVEMENT; //if there is a piece in the way
			}

			isPosEq = (tmp == dstPos);
		}


		return response::succes::MOVEMENT;
	}
	catch(exception)
	{
		return response::error::ILLEGAL_MOVEMENT;
	}
}

